Since TCP is a connection-oriented protocol, it tries to setup a connection with
the other peer before any data exchange, and at the end of the transmission, it
peacefully closes it. The objective is
to deliver an ordered stream of bytes between two end points. That flow is
bi-directional (data can be exchanged both ways), and is governed by a state machine
outlined in RFC 793.

Since the data stream must be delivered in order to the application, without
holes, if some segments are lost they must be retransmitted. End
hosts exchange control packets (that can be piggy-backed with application data)
between themselves, to regulate the data flow: the main is the acknowledgment
(ACK), which acknowledge one (or more) successful receipt(s). There are different
strategies to sense a packet loss, through timer expiration (Retransmission
TimeOut, RTO) or by inferring such loss by receiving multiple duplicated ACK
segments (Fast Retransmission).

At this point, to characterize the network data load in the presence of TCP flows,
it is worth to note that the amount of data injected into the network is not
following a constant-rate pattern, but is instead defined by an internal state
variable referred to as congestion window (\emph{cWnd}) that changes over time.
It is decreased after each congestion event, and it is increased, depending on
the connection history, by either slow start or congestion avoidance phases.
The slow start phase is commonly used unaltered by all TCP implementations (please note
that, contrarily to its name, the \emph{cWnd} growth in this phase is
exponential but depends on the connection RTT). Congestion
avoidance algorithm enters the game when the \emph{cWnd} exceeds the slow start
threshold; historically, the Reno and Tahoe algorithms were the first deployed.
Right now, the algorithm on the Standard Track (RFC 5681 and RFC 6582) is New
Reno (loss-based), but many variants have been proposed to overcome general and
environment-specific problems and performance issues, and some of them even
define the \emph{cWnd} reduction strategy after a congestion event. The TCP
congestion avoidance variants can be divided following the main parameter that
signal congestion: \emph{loss-based} variants use the packet loss information
(guessed through duplicated ACK or timeout) as the primary congestion signal,
while \emph{delay-based} variants use the queuing delay as the primary congestion
signal, increasing window size if delay is small and decreasing it if delay is
large.

For sake of completeness, we report in the following the main
characteristics of the loss-based congestion control algorithms used in this paper:

\begin{itemize}
%  \item \emph{Bic} views the congestion avoidance phase as a search problem:
%    taking as a starting point the current window value and as a target point
%    the last maximum window value, a binary search technique is used to update
%    the \emph{cWnd} value at the midpoint between the two, directly or using an
%    additive increase strategy if the distance from the current window is too
%    large;
  \item \emph{New Reno} is the evolution of Reno algorithm, where the window
    growth depends on the RTT;
  \item \emph{Cubic} is an algorithm for high-speed network environment. The
    window growth function is governed by a cubic function in terms of
    the elapsed time since the last loss event, and thus is independent from
    the RTT;
  \item \emph{HighSpeed} is designed for TCP connections with large congestion
    windows, and its algorithm kicks in when the \emph{cWnd} increases beyond
    a pre-defined threshold, allowing faster growths and accelerating the recovery
    from losses;
%  \item \emph{Veno} enhances New Reno algorithm for effectively dealing with
%    random packet loss in wireless access networks, by employing Vegas's method
%    in estimating the backlog at the bottleneck queue, in order to distinguish
%    between congestive and non-congestive states;
  \item \emph{Westwood} is based on New Reno but uses the AIAD (Additive Increase/Adaptive Decrease)
    approach: when a congestion episode happens, instead of halving
    the \emph{cWnd}, it tries to estimate the network bandwidth and use
    the estimated value to adjust the cWnd;
  \item \emph{Hybla} is born for satellite connections, and the key idea is to
    obtain, for these connections, the same instantaneous transmission rate of
    a low-RTT reference connection;
  \item \emph{Scalable} improves the bandwidth utilization by employing a more
    aggressive adjustment algorithm, with respect to New Reno.
\end{itemize}

For the delay-based family, we employed \emph{Vegas}, a pure delay-based
congestion control algorithm. It implements a proactive scheme which tries to
prevent packet drops by maintaining a small backlog at the bottleneck queue.

% TCP on Linux (NAT)
%Right now, Cubic~\cite{ha2008cubic} is used as default congestion avoidance algorithm
%in systems equipped with the Linux kernel. One of its main strength is that,
%unlikely the old (and often used as reference) New Reno algorithm, its window
%growth depends only on the real time between two consecutive congestion events.
%Thus, the window growth becomes independent of Round-Trip-Time (RTT) measurements,
%allowing to efficiently utilize resources even in the presence of high delays.
%The smoothed RTT calculation is defined in RFC 6298 and it is also used to
%calculate the value of RTO.

% TCP on Wireless Backhaul (NAT)
%Transmission Control Protocol (TCP) has adopted in the Internet as the de-facto
%standard for reliable, connection-oriented and in-order end to end data
%delivery. In recent years, wireless scenarios have created many challenges for
%TCP researchers, both in infrastructure-based and mobile ad-hoc wireless
%networks. However, it is difficult to export research results to consumers'
%world. In fact, Android-based devices (that represents a large portion of end
%users devices) use a not-so-recent version of the Linux operating system, and
%even if its TCP implementation is one of the most advanced in the world,
%nevertheless it is based on the same principles that many papers propose to
%change (slow start, congestion avoidance, fast retransmission and fast
%recovery) to enhance TCP performance on these challenging scenarios.
%Summarizing, the TCP performance in the scenarios that are relevant for this
%paper are limited by:
%
%\begin{itemize}
%  \item Packet loss. After each loss guessed through the three duplicated ACK
%    strategy (Fast Retransmission) or the Retransmission TimeOut expiration
%    (RTO), for an implicit design assumption which states that losses are due
%    to congestion, TCP slows down its rate and retransmits the (guessed) lost
%    segment(s).
%
%  \item Multi-path Routing. If packets of the same flow are routed over
%    different paths, they may not arrive at the destination in-order. Since TCP is
%    unaware of multi-path routing, the receiver would misinterpret such
%    out-of-order packet arrivals as congestion, generating duplicate ACKs and
%    triggering Fast Retransmission.
%\end{itemize}

% Don't know where it belongs (NAT)
% received out-of-order. Each out-of-order segment is signalled to the TCP sender,
% through duplicated ACK, and the sender slows down its rate after three
% of them due to the Fast Retransmission algorithm. Generally speaking, using
% intensively the multi-path feature of some routing protocol highlights
% critical issues in TCP implementation, and how it detects spurious
% retransmissions (RFC 3708) impact on the overall performance: more the
% implementation can ``roll off'' its wrong decisions (e.g. a packet flagged as
% lost was simply reordered) more the TCP rate control can couple with rate
% control of routing algorithms.
