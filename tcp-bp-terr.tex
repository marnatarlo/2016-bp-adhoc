\documentclass[review]{elsarticle}

\usepackage{lineno,hyperref}
\usepackage{listings}
\usepackage{multicol}
%\usepackage{url}
\usepackage{breakurl}

\modulolinenumbers[5]

\journal{Ad Hoc Networks}

%%%%%%%%%%%%%%%%%%%%%%%
%% Elsevier bibliography styles
%%%%%%%%%%%%%%%%%%%%%%%
%% To change the style, put a % in front of the second line of the current style and
%% remove the % from the second line of the style you would like to use.
%%%%%%%%%%%%%%%%%%%%%%%

%% Numbered
\bibliographystyle{model1-num-names}

%% Numbered without titles
%\bibliographystyle{model1a-num-names}

%% Harvard
%\bibliographystyle{model2-names.bst}\biboptions{authoryear}

%% Vancouver numbered
%\usepackage{numcompress}\bibliographystyle{model3-num-names}

%% Vancouver name/year
%\usepackage{numcompress}\bibliographystyle{model4-names}\biboptions{authoryear}

%% APA style
%\bibliographystyle{model5-names}\biboptions{authoryear}

%% AMA style
%\usepackage{numcompress}\bibliographystyle{model6-num-names}

%% `Elsevier LaTeX' style
%\bibliographystyle{elsarticle-num}
%%%%%%%%%%%%%%%%%%%%%%%


\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{url}

\usepackage{tikz}
\usepackage{epstopdf}
\usepackage{verbatim}
\usepackage{balance}
\usepackage{comment}
\usepackage{enumerate}
\usepackage{graphicx}
\usepackage{caption}
\usepackage[normalem]{ulem}

% correct bad hyphenation here
\hyphenation{New-Reno li-mi-ted va-rio-us nu-me-ri-cal acce-le-ra-tes  lo-ga-rith-my-cal-ly fol-lo-wing noord-wijk Noord-wijk va-lues exa-ctly  ty-py-cal o-ri-gi-nal-ly spe-ci-fy par-ti-cu-lar lo-ga-rithmi-cal-ly  beha-vi-or}

\begin{document}
\begin{frontmatter}

%\title{TCP Performance Evaluation on Backpressure-based Routing Strategies within Wireless Backhaul for LTE Networks\tnoteref{ack}}
\title{TCP Performance Evaluation over Backpressure-based Routing Strategies for Wireless Mesh Backhaul in LTE Networks\tnoteref{ack}}

\tnotetext[ack]{This work was partially funded by the EC under grant agreement no 645047 (H2020 SANSA project) and by the Spanish Ministry of Economy and Competitiveness under grant TEC2014-60491-R (5GNORM).}

%% Group authors per affiliation:
\author[ing]{Natale~Patriciello}
\ead{natale.patriciello@unimore.it}

\author[cttc]{José~N\'uñez-Mart\'inez}
\ead{jose.nunez@cttc.cat}

\author[cttc]{Jorge~Baranda}
\ead{jorge.baranda@cttc.cat}

\author[ing]{Maurizio~Casoni}
\ead{maurizio.casoni@unimore.it}

\author[cttc]{Josep~Mangues-Bafalluy}
\ead{josep.mangues@cttc.cat}

\address[ing]{Department of Engineering Enzo Ferrari \\ University of Modena and Reggio Emilia \\ via Vignolese 905, 41125 Modena, Italy.}

\address[cttc] {Centre Tecnològic de Telecomunicacions de Catalunya (CTTC) \\ Av. Carl Friedrich Gauss 7, \\ 08860 Castelldefels, Barcelona, Spain}


\begin{abstract}
Wireless redundant networks are expected to play a fundamental role to backhaul
dense LTE networks. In these scenarios, backpressure-based routing strategies
such as \emph{BP-MR} can exploit the network redundancy. In this paper, we
perform an exhaustive performance evaluation of different TCP variants over an
LTE access network, backhauled by various routing protocols (including
\emph{per-packet} and \emph{per-flow} \emph{BP-MR} variants and a static
alternative, \emph{OLSR}) over two different wireless topologies: a regular
mesh and an irregular ring-tree topology. We compare the performance of
different TCP congestion control algorithms based on loss (NewReno, Cubic,
Highspeed, Westwood, Hybla, and Scalable) and delay (Vegas) under different
workloads. Our extensive analysis with ns-3 on throughput, fairness,
scalability and latency reveals that the underlying backhaul routing scheme
seems irrelevant for delay-based TCPs, whereas \emph{per-flow} variant offers
the best performance irrespective of any loss-based TCP congestion control, the
most used in the current Internet. We show that \emph{BP-MR per-flow} highly
reduces the download finish time, if compared with \emph{OLSR} and \emph{BP-MR
per-packet}, despite showing higher round-trip-time.
\end{abstract}

\begin{keyword}
TCP\sep Backpressure \sep ns-3 \sep Routing \sep LTE
\end{keyword}

\end{frontmatter}

\linenumbers

\section{Introduction}
\label{sec:introduction}
\input{introduction}

\section{BP-MR and TCP Background}
\label{sec:background}
\subsection{BP-MR (per-packet and per-flow)}
\input{per-flow}
\subsection{TCP and Congestion Control Algorithms}
\input{tcp}

%\section{Methodology}
%\label{sec:methodology}
%\input{methodology}

\section{Analyzing regular backhaul topologies}
\label{sec:mesh_performance}
\input{tcp-comparison-fix-udp}

\section{Analyzing irregular backhaul topologies}
\label{sec:modena_performance}
\input{tcp-comparison-modena}

% \section{Recommendations and Lessons Learn}
% \label{sec:recom}
% \input{recom}

\section{Related work}
\label{sec:related}
\input{related}

\section{Conclusion}
\label{sec:conclusion}

This paper extensively evaluates throughput, fairness, scalability, and latency
of different TCP congestion control algorithms (TCP New Reno, Cubic, Highspeed,
Westwood, Hybla, Scalable, and Vegas) in the context of current and future
wireless mesh backhauls carrying LTE traffic. We analyzed fixed-path and
different backpressure-based routing strategies : \emph{OLSR}, \emph{BP-MR}
\emph{per-packet}, and the \emph{BP-MR} \emph{per-flow} variant. Experiments
conducted with ns-3 revealed some findings regardless of the backhaul topology
under evaluation. First, simulations with delay-based TCP Vegas, being
conservative, are incapable of stressing the backhaul yielding into similar
performance over the three analyzed routing strategies.

Second, loss-based variants such as Cubic (which is used by default by the
Linux operating system) pose difficulties to the routing protocols, due to
their aggressiveness, and then differences in the performance are clearly
visible independently from the variants themselves. With loss-based TCP,
\emph{BP-MR} \emph{per-flow} decisions offer the best flow performance,
regardless of the TCP variant, despite its higher round-trip-time irrespective
of the backhaul topology under evaluation.

\subsection{Lessons Learned}
The evaluations presented let us state the following generic recommendations:

\begin{itemize}
  \item Wireless backhaul deployments requires flexible routing protocols, in
    order to evenly exploit backhaul redundant resources;
  \item There is a trade-off between routing flexibility and TCP performance.
    We have shown how per-packet routing decisions (highest degree of load
    balancing for system efficiency) can be good for UDP traffic, but it
    experiences the worst results for TCP traffic (with reliability and
    ordering requirements);
  \item The per-flow protocol, being able to trade-off between even resource
    usage and traffic patterns requirements, is the one getting the best
    results in terms of throughput and fairness;
  \item The aforementioned results are generalizable to multiple topologies,
    spanning from current backhaul deployments of eNodeBs (e.g., Modena
    topology) to future dense SC mesh deployments;
\end{itemize}

\subsection {Future Work: BP-MR per-burst}
Future works include (but are not limited to) the investigation of another
variant of \emph{BP-MR}, which takes decision on a \emph{per-burst} basis, to
have some degree of flexibility (e.g., in case of long-lasting flows) without
incurring in the reordering penalization typical of \emph{BP-MR per-packet}, to
be tested on different topologies with different degree of randomness.

\section*{References}

\bibliography{tcp-bp-terr}

\end{document}
