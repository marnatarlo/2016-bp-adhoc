Wireless mesh backhauls are expected to play a fundamental role in providing transport
resources closer to the edge and the capillarity required by next generation
mobile networks. In particular, for accommodating the rising demand for wireless
connectivity, future Long Term Evolution (LTE) dense deployments formed by
Small Cells (SC) are expected to be deployed, because reducing cell size to
increase frequency re-use is the most simple and
effective way to increase raw capacity~\cite{webb2007wireless}. Therefore, due
to fiber unavailability in all the deployment scenarios, backhauling these
dense LTE deployments can be addressed with wireless (e.g., mmWave) transport
nodes associated to SCs. Each of the wireless transport nodes will have
multiple backhaul interfaces, with the possibility to create a backhaul network through a
redundant wireless mesh. In this way, LTE user and control plane
traffic will be appropriately carried from/towards the Evolved Packet Core
(EPC), with major benefits regarding cost, ease of deployment, coverage, and
capacity.

The path redundancy of these potentially large wireless mesh networks should be
exploited by the backhaul routing protocol since it determines the way data is
transported to/from the UEs. In literature, we have congestion-agnostic
strategies such as Multiprotocol Label Switching
(MPLS, RFC 5921) and Optimized Link State Routing (OLSR, RFC 3626). There are also
congestion-aware strategies, such as backpressure-based ones~\cite{7422412},
which can exploit network redundancy because they dynamically map the
trajectory followed by each packet to the less utilized path. However, these
decisions may potentially make the path followed by consecutive packets of the
same flow disjoint.

Today, one of the most appropriate protocols to transport traffic is TCP, thanks
also to the increasing popularity of on-line streaming services (e.g., YouTube,
Netflix, and Spotify). Many TCP variants have been proposed over the years,
with the declared objective of fixing inefficiencies of the standard version
over peculiar environments (e.g., high bandwidth-delay product, different access
channels, ...). While TCP performance over Radio Access Network (RAN) such
as LTE, has been analyzed in the past, a fundamental question is whether
backhaul routing protocols can also appropriately serve TCP traffic, and
moreover if they penalize or reward different TCP congestion control algorithms
in the mentioned wireless mesh backhaul deployments. The challenge for backhaul
routing protocols is clearly to exploit the network redundancy without
affecting the TCP performance. 

The contribution of this paper is threefold: (1) to investigate the advantages
and limitations of different backhaul routing strategies under a wireless mesh
backhauls carrying LTE traffic; (2) to analyze the particular response of
different TCP congestion control algorithms over these backhaul routing
protocols; and (3) to infer the proper combination of TCP congestion control
and underlying backhaul routing protocol within the aforementioned context.

Regarding backhaul routing protocols we compare well-known single-path \emph{OLSR},
which in the absence of node mobility and failures is equivalent to MPLS for the purpose of the paper,
with routing variants based on backpressure routing concept. In this sense,
our starting point is Backpressure Multi-Radio (BP-MR)~\cite{7396680}, which
operates on a \emph{per-packet} basis. To reduce the reordering at destination,
we also employ a \emph{per-flow} variant of BP-MR, proposed
in~\cite{en4ppdrrouting} but tested only in an emergency scenario with a
backhaul satellite link. We evaluate the response of these
three backhaul routing variants using TCP Cubic between the end-points of
the communication.

Afterwards, the paper broadens the analysis of the mentioned backhaul routing
strategies considering the impact of installing different loss- and delay-based
TCP congestion control algorithms (i.e. New Reno, Highspeed, Westwood, Hybla,
Vegas, and Scalable) in the UEs and the remote server. Last but not least, we
analyze in detail the different response of delay- and loss-based TCP
congestion control algorithms over the different routing strategies in the
presence of an increasing UDP backhaul workload.

The conducted experiments with ns-3\footnote{https://www.nsnam.org} on two different topologies analyzing
throughput, fairness, scalability, and latency allow concluding that
delay-based TCP variants, such as Vegas, are unable to stress the backhaul
resources achieving similar performance over the three analyzed routing
strategies, but they are outperformed by loss-based TCP variants, such as
Cubic. However, these loss-based congestion control algorithms pose difficulties to
routing protocols due to their aggressiveness in sending data.
The use of different routing algorithms is then reflected by
differences in TCP performance, irrespective of the TCP congestion control
algorithm used. Specifically, using \emph{OLSR} as the reference protocol,
\emph{BP-MR per-packet} exhibits lower fairness among flows and lower throughput due to
required packet reordering at the destination. On the other hand,
\emph{BP-MR per-flow} offers the best TCP performance by reducing the download finish time
despite its higher round-trip-time. The performance improvement is due to fewer losses and
reordering events experienced while showing capabilities to circumvent
congestion on a \emph{per-flow} basis.

The remainder of this paper is organized as follows.
Section~\ref{sec:background} contains the necessary background on \emph{BP-MR}
(both \emph{per-packet} and \emph{per-flow}) and TCP protocol.
Section~\ref{sec:mesh_performance} describes the methodology and the results
gathered over a regular mesh backhaul topology. In
Section~\ref{sec:modena_performance} we present the results collected on an
irregular topology, taken from a real eNodeB deployment in the city of Modena,
Italy. Section~\ref{sec:related} covers related work, and finally,
Section~\ref{sec:conclusion} concludes the paper.
