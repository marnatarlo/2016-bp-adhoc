\subsection {Methodology}

We modeled a 10 MB file transfer from a remote server outside the Mobile
Network to the UEs, which represents the download of a data chunk from a remote
streaming service. In all the simulations, there is always one transfer per UE
attached to the Mobile Network. Therefore the number of TCP file
transfers is equivalent to the number of UEs. To complete the picture,
the UEs are uniformly distributed over the eNodeBs.

We conducted experiments for different TCP variants; we compared loss-based
flavors TCP Cubic, NewReno, HighSpeed, Westwood, and Hybla, with Vegas, a TCP
flavor based on delay to calculate congestion window. Furthermore, for each
TCP flavor, we compared the performance of different underlying backhaul
routing protocols: single-path based on \emph{OLSR}, and backpressure
\emph{per-packet} and \emph{per-flow} based on \emph{BP-MR}. All the simulations are
conducted with ns-3 using latest version of LTE
model\footnote{http://networks.cttc.es/mobile-networks/software-tools/lena/}, \emph{BP-MR}
routing protocol implemented in~\cite{7396680}, and the different TCP variants
presented in~\cite{casoni2016next}.

We measure two parameters to quantify the performance of the TCP connections:
the download finish time and the average RTT. The download finish time
quantifies the average throughput of the end-to-end connection (i.e., from the
remote server to the UEs). The RTT abstracts network latency and is defined as
the time taken by a TCP segment to reach the UE plus the time taken by the
server to receive the correspondent ACK from the UE. This parameter is gathered
from the TCP data sender each time it receives a non-duplicated ACK segment:
therefore, retransmitted or out-of-order data packets are not taken into
account in the RTT measurement. Thus, the RTT should be weighted with the
download finish time to properly characterize the performance experienced by
TCP in combination with each of the routing protocols under evaluation. The
combinations of download finish time and RTT will give insights on throughput,
fairness, and latency.

The reported values of download time and RTT are represented in candlesticks,
where the boxes stretch from the 20th to the 80th percentiles, and the
whiskers represent the maximum and minimum values, with the average value
represented by a black horizontal line. In the following, we analyze
the routing protocol impact, the TCP congestion control impact,
and finally the different response of the routing protocols to loss-based and
delay-based TCP.

\begin{figure}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.95\textwidth]{figures/network_scenario_src/NetworkScenario.eps}
\caption{Full mesh topology (UE number can vary).}
\label{fig:scenario}
\end{figure}

Our reference scenario depicted in Figure~\ref{fig:scenario} is a dense SC
network deployment covering $2 Km^{2}$ with an inter-SC distance of two hundred
meters. Note that every SC is composed of an LTE eNodeB and a wireless
transport node. In particular, twenty-five LTE eNodeBs are deployed with peak
downlink throughput of 350 Mb/s that corresponds to an areal capacity of
$8.8\frac{Gbps}{Km^{2}}$~\cite{idcc}. The wireless transport node endows
several 500 Mb/s point-to-point (PTP) interfaces that form wireless links of 4
ms of propagation delay and are connected among them to form a plain grid, as
illustrated in Figure~\ref{fig:scenario}. The mesh is in turn connected to the
LTE Evolved Packet Core (EPC) through three PTP wired links, with 1 Gb/s of
available bandwidth and 0.5 ms of propagation delay. The EPC is connected to
the Internet through another PTP wired link (characterized by a bandwidth of 10
Gb/s and a propagation delay of 5 ms); the queue sizes of the backhaul network
are set to the 100 \% of each link bandwidth-delay product, making an effort to
reduce default buffer size, as suggested in~\cite{jamshaid2014deflating}. Regarding the Radio
Access Network (RAN), we used European frequencies and a Okumura-Hata
propagation model~\cite{bandePPDREU}: the LTE connection between UE and the
eNodeB is modeled inside an urban environment of a medium city.

\subsection {Backhaul Routing Protocol Impact}
\begin{figure*}
\centering
\captionsetup{justification=centering}
%\includegraphics[width=.95\textwidth]{figures/plot/TCP-comparison/25/25UE-download-1Mbps.eps}
\includegraphics[width=.95\textwidth]{figures/plot/UE-increase/TcpCubic-UE-increase-download-1Mbps.eps}
\caption{RTT and file download time using TCP Cubic, while increasing the number of LTE UEs downloading a 10 MB file.}
\label{fig:evaluation_cubic}
\end{figure*}

Here we analyze the backhaul routing protocols response when using TCP Cubic
(the default congestion control algorithm of Linux,
and hence of Android-based devices and the majority of servers on the Internet)
as default transport protocol for the transfers. Briefly, we remind that
\emph{OLSR} always chooses the shortest path in terms of hops, \emph{BP-MR
per-packet} evaluates for each packet the best trade-off between proximity and
congestion, while \emph{BP-MR per-flow} performs the same proximity-congestion evaluation
but for each flow.

\textbf{Throughput.} Figure~\ref{fig:evaluation_cubic} shows the TCP
download time performance over four different cases, namely with 25, 50,
75, and 100 UEs concurrently downloading a remote file. Results reveal that
by using \emph{BP-MR per-flow} the flows experience the smallest file download times.
Moreover, \emph{BP-MR per-packet} experiences a significant throughput
degradation because it has the higher times of the entire set of
protocols, whereas \emph{OLSR} is between the two. The bigger size of the
boxplots in per-packet indicates a frequent use of unconstrained path
diversity, causing excessive reordering at UEs.  The number of potential
trajectories that a packet can take with \emph{BP-MR per-packet} is unconstrained
since (i) independent decisions are taken in every hop and (ii) routing decisions
are independent for consecutive segments in each hop. Maximum values obtained
with \emph{per-flow} are around the average obtained with \emph{BP-MR per-packet}
indicating a frequent use of redundant paths in \emph{BP-MR per-packet}.

\textbf{Fairness.} By looking at the size of the download time boxplots in
Figure~\ref{fig:evaluation_cubic} we can reveal the degree of fairness between
concurrent TCP flows (more tight they are, more fair are the flows).
\emph{BP-MR per-packet} exhibits a high degree of variability, indicating a
poor fairness between parallel TCP file transfers. In this case, fairness is
sacrificed in an attempt to make the most out of the network resources blindly
by taking routing decisions on a per-packet basis without caring about segment
ordering at the destination. \emph{OLSR} also exhibits a lower fairness
degree compared to \emph{BP-MR per-flow} that increases with the number of
concurrent file transfers, given its trend to prioritize file downloads
launched by UEs closer in terms of hops to the EPC.

\textbf{Response to an increasing load.} Increasing the number of concurrent
downloads, we can see that the download time increases for all the variants. The
degree of such increase, however, is different. Moreover we can see for
\emph{OLSR} a decreased fairness, with the candlesticks that dramatically widen
from 25 to 100 UE. For what regards \emph{BP-MR per-packet}, that degree of
wideness is reduced, while with \emph{BP-MR per-flow} practically we have the
same performance between 75 and 100 UE, but the fairness decreases from the 25
and 50 UE cases.

\textbf{Latency.} Looking to the RTT values of
Figure~\ref{fig:evaluation_cubic}, we can see that \emph{BP-MR per-flow}
experiences the highest RTT values. This can be explained by the fact that
these flows have a lower probability to experience RTOs and reordering, and
then more samples can be collected (as we described in the Methodology
subsection). For \emph{OLSR} and \emph{BP-MR per-packet} we have lower RTT
values, with \emph{OLSR} performing slightly better, but higher finish time, as
we said before. With more time to conduct a file transfer, both routing
protocols are more likely to exhibit lower RTT samples, thanks to the slow
probing phase that follows a congestion event, since these packets will
encounter less congestion along the way.

\textbf{Insights.}\emph{OLSR} suffers when increasing the load, and generally
has no defense against congestion but the fixed path selection gives in-order
delivery and practically constant delays.
On the other hand, \emph{BP-MR per-packet} dynamically avoid congestion,
but leads to a high degree of reordering, worsening the TCP
performance with respect to \emph{OLSR}. The best results are obtained with
\emph{BP-MR per-flow}, that blends the best from the other two strategies: fixed
paths and congestion avoidance. In the following, we investigate if these
insights are also valid when employing different TCP congestion control
algorithms.

\subsection{TCP Congestion Control Impact}
\begin{figure*}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.95\textwidth]{figures/plot/TCP-comparison/100/100UE-download-1Mbps.eps}
\caption{File download time and RTT for different TCP, with 100 UEs performing a 10 MB download}
\label{fig:evaluation_100}
\end{figure*}

To analyze the impact of TCP congestion control algorithm over the
overall performance obtained through the routing protocol strategies, we
will refer to Figure~\ref{fig:evaluation_100}, which contains results achieved
with 100 user devices connected.

\textbf{Throughput.} The smallest average finish time, considering all the routing
protocols, is achieved with Scalable, followed by Highspeed and Hybla variants. Contrarily to what we
could have expected giving its predominance on the Internet, Cubic is
not performing well: in this particular case, the average value indicates that
its performance is comparable with the New Reno variant, and moreover Cubic
presents the highest download time for one, unlucky, flow. In average, however,
the worst throughput performance is achieved by Vegas.

\textbf{Fairness.} Analyzing the wideness of the boxes, the most fair TCP congestion control
algorithm is Scalable, that presents very tight boxes for all the routing
protocols. Other algorithms seem to suffer more, with Westwood that presents the
worst fairness indicator. In general, the fairness is increased through the use of
the \emph{BP-MR per-flow} routing protocol.

\textbf{Latency.} TCP Vegas is clearly offering a practically constant RTT,
regardless of the routing protocol. This value is also the smallest registered,
due also to the low throughput,
emphasizing the delay-based mechanism of Vegas: in the next subsection we will
analyze in depth the Vegas behavior. On the other hand, Scalable
and Highspeed present the higher average values, near the 100 ms mark. We would
like to remark the fact that this value indicates the time that, in average, is
required from the moment that a segment leaves the TCP buffer to the time that
its ACK is received by the sender node.

\textbf{Congestion control characteristics.} In the following, we briefly recap
the most important result for each congestion algorithm:

\begin{description}
  \item \emph{NewReno} does not suffer the competition with other loss-based variants,
    in this scenario. For instance, the average RTT and finish time average values are
    slightly better than Cubic for all the routing protocols;
  \item \emph{Cubic} puts a lot of stress on the routing protocol, and in fact, its average RTT
    value is higher than Westwood, Hybla, NewReno, and Vegas. Neither in the
    finish time it presents better results, performing worse than New Reno with
    all the routing protocols;
  \item \emph{Highspeed} presents slower finish time than other versions (except for
    Scalable), but presents a really different performance giving different
    routing protocols, achieving the best finish time with \emph{per-flow};
  \item \emph{Westwood} thanks to its bandwidth estimation approach is able to maintain
    lower RTT, but this reflects on the worsening of the performance related to
    the finish time;
  \item \emph{Hybla} obtains similar performance as Highspeed, giving that the low RTT
    of the network is not permitting to exploit its characteristics;
  \item \emph{Vegas} is more conservative throughput-wise, but achieves the best results
    in terms of RTT. This conservativeness implies smaller congestion window;
  \item \emph{Scalable} does not show a clear improvement in terms of RTT from NewReno, but it shows
    tight boxes for the finish time, and so demonstrating itself as the fairest.
\end{description}

\textbf{Routing protocol trend remarks.} We can observe that the trend outlined in the
previous subsection for the different routing variants is also valid for other
loss-based congestion control algorithms than Cubic. Regardless of the
employed TCP: \emph{OLSR} provides constant delays but is not fair (wide boxes)
and not optimal throughput-wise; \emph{per-packet} has really high finish times,
and it is highly unfair between different parallel TCP file transfers, while
\emph{per-flow} exhibits lower file finish times at the expenses of an increased
RTT. From this information, merged with the results presented in this
subsection for different TCPs, we can conclude that for loss-based TCP \emph{BP-MR
per-flow} significantly improves the TCP performance, more than the particular TCP variant
under evaluation. A different conclusion should be drawn for Vegas, which is
delay-based. The performance for RTT and throughput are practically the same,
regardless of the routing protocols.

\textbf{Insights.} Overall, loss-based TCP variants finish their download
earlier than delay-based TCP variants (Vegas), or variants that use a bandwidth
estimation (such as Westwood). This higher amount of bytes in-flight reflects on
the RTT, which is generally higher in more aggressive variants, regardless of
the routing protocol.  The broad literature on the subject supports the obtained
result. By contrast with wired networks in redundant topologies the routing
protocol matters, as we have seen. This suggests that TCP protocols evaluation
should always be done by keeping in mind the underlying routing protocol. At
this point, it is interesting to analyze in detail the Vegas and the Highspeed
variants, representative of delay-based and loss-based categories, respectively,
to see the differences in the congestion window evolution and if (and how)
different routing strategies are influencing their internal mechanism.

\subsection{Delay-based versus loss-based TCP variants}
\begin{figure}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.95\columnwidth]{figures/plot/TCP-comparison/cwnd-comparison-25/TcpVegas-cwnd-1Mbps-UE3.eps}
\caption{Vegas cWnd evolution for one UE}
\label{fig:vegas}
\end{figure}

\begin{figure}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.95\columnwidth]{figures/plot/TCP-comparison/cwnd-comparison-25/TcpHighSpeed-cwnd-1Mbps-UE6.eps}
\caption{HighSpeed cWnd evolution for one UE}
  \label{fig:highspeed}
\end{figure}

As stated before, to better understand the differences between delay-based and loss-based TCP
variants on the different routing strategies, we choose one representative congestion window
evolution for two TCP variants: Vegas (delay-based) in Figure~\ref{fig:vegas} and
HighSpeed (loss-based) in Figure~\ref{fig:highspeed}.

\textbf{TCP Vegas, delay-based.} For what regards Vegas, its delay-based
mechanism for increasing the congestion window maintains the cWnd value under
the 100 KB threshold except for \emph{OLSR} (that approaches the 200 KB threshold), and
its growth is very slow (please note that the y-axis
is logarithmic). This does not create congestion, and the different routing
algorithms are not stressed; hence, the TCP behavior on top of them is
practically the same. Unfortunately, the slow growth and the limited values are
reflected also in the download time (and so in the throughput), which is higher
than the other variants. An interesting property to analyze is if the routing
protocol influences the delay-based mechanism at the root of Vegas. Indeed,
Vegas depends on the measured RTT value, and so theoretically a \emph{per-packet}
strategy (in which each packet has potentially a very different RTT) should
penalize the algorithm itself. However, without any other congestion source,
the slow growth of Vegas does not introduce any congestion, even with 100
devices, and the \emph{per-packet} RTT is quite similar to the values achieved with
other strategies. For sure, if the RTT values start to be very different (e.g.
already present congestion) all the mechanisms based on the RTT will be very
penalized. In general, it is widely known that the throughput of delay-based
methods is worse than the throughput of more aggressive variants, but adding
multi-path routing protocols on the backhaul network is worsening the situation,
penalizing them more than loss-based strategies.

\textbf{Highspeed, loss-based.} On the other hand, loss-based HighSpeed variant
produces some stress on the routing protocols, and each one is responding
differently, producing different shapes of the congestion window. Congestion
events with \emph{OLSR} are due to packet losses, clearly visible from the drastic
decrease of the window that is happening near the same y-value. In fact, each
hop is using a drop-tail queue that, after being filled by the
amount of in-flight data, produces losses, actually limiting the sender.
\emph{BP-MR per-packet} experiences the lowest congestion window absolute value,
and it drops randomly during the simulation. This is due to high degree of
reordering introduced by the strategy itself. The reordering is happening for
the aggressiveness of the TCP variant, seen by the backpressure strategy as
congestion. As a consequence, we can see that the transmission needs longer time
with respect to \emph{OLSR} to complete. Finally, with \emph{BP-MR per-flow} variant,
the reordering is not happening anymore, but still, the flow can avoid congested
paths; the congestion window can grow to a more appropriate value, before
being limited by the losses due to a buffer overflow, in the same way as \emph{OLSR}.
Overall, this allows \emph{BP-MR per-flow} to reduce the transmission time with respect to
both \emph{OLSR} (thanks to the congestion avoidance strategy of the routing protocol
itself) and \emph{BP-MR per-packet} (thanks to avoiding reordering at the destination, by
keeping the flow path fixed).

\subsection{Workload Impact}
\begin{figure}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.95\columnwidth]{figures/plot/UE-fix/100/TcpVegas-download-finishtime-100UE-panoramic.eps}
\caption{Time required to perform a 10 MB download from a remote node using TCP Vegas under different backhaul workload conditions}
\label{fig:vegas_load}
\end{figure}

\begin{figure}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.95\columnwidth]{figures/plot/UE-fix/100/TcpHighSpeed-download-finishtime-100UE-panoramic.eps}
\caption{Time required to perform a 10 MB download from a remote node using TCP Highspeed under different backhaul workload conditions}
\label{fig:highspeed_load}
\end{figure}

At this point, it is interesting to know if the behavior of the delay-based or
loss-based variants remains the same when increasing the workload on the mesh.
To do so, we modeled existing LTE traffic as a constant-rate UDP traffic, composed by
different flows directed among random SC source-destination pairs. In the
following, we will refer to the aggregated UDP traffic in the entire mesh
network, instead of characterizing throughput performance of every single UDP
flow. We will not consider latency of these flows because an in-depth
analysis of UDP traffic within the \emph{BP-MR} framework was already conducted
in~\cite{7396680}.

\textbf{TCP Throughput.} In Figure~\ref{fig:vegas_load} it is reported the
download finish time statistics (over a 10 MB file) of 100 UEs employing TCP
Vegas, increasing the aggregated UDP traffic from 60 Mb/s to 1200 Mb/s.
The TCP performance on top of \emph{OLSR} and \emph{BP-MR per-flow} are practically
the same, with some small differences between the two on the higher whisker.
\emph{BP-MR per-packet} is generally performing worst, with wide boxes and generally an
higher average values. By comparing the 1200 Mb/s case with the result for Vegas
reported in Figure~\ref{fig:evaluation_100}, we can state that the general trend
is the same, or in other words the increased load on the backhaul is not
reflected in the behavior of Vegas on top of the analyzed routing strategies.

Even for loss-based TCP, the situation does not change. From
Figure~\ref{fig:highspeed_load}, where we plot the download finish time
statistics employing TCP Highspeed, the trend does not change while increasing
the backhaul load. However, a small difference between this result and the
previous with Vegas is that, even if very slowly, the average download time
increases with the increase of the load. This is better visible by looking at
the higher whisker of each case, that represent the worst download time: for
OLSR and \emph{BP-MR per-flow} it increases with the load, except for some situations in which the
randomness of the simulation is coming into play (even if simulations are
repeatable, changing the backhaul load implies different moments on which
drops occur, changing the outcome by a small delta, but without changing the
overall picture). On the other hand, we have practically a constant trend for
\emph{BP-MR per-packet}, where the reordering problem has a bigger impact on the
performance rather than queue drops. By comparing the 1200 Mb/s case with the
results obtained for Highspeed in Figure~\ref{fig:evaluation_100}, is clearly
visible how the routing protocol affects the performance of the TCP flows in the
same way, regardless of the backhaul load, like in the delay-based case.

\textbf{UDP Throughput.} For both Figures~\ref{fig:vegas_load}
and~\ref{fig:highspeed_load}, we plot on the right side the average UDP
throughput. The value is reported as a percentage of the measured traffic at the
endpoints over the injected traffic. The routing protocols can deliver
more than the 95\% of the traffic (with \emph{per-flow} and \emph{per-packet} up to 100\%).
The reason of the lower result of \emph{OLSR} is in the time it requires to populate the 
routing tables of each backhauling node, and so zeroing the throughput for the very first
seconds of the simulation. The result is derived from the high competition that
is going on between the TCP and UDP flows. The 100 TCP flows are increasing
their congestion window slowly, and since UDP constant rate is unresponsive
(i.e. it does not reduce the traffic after congestion event) it is not overall
penalized by the routing strategies, even after the saturation point (in which,
without any scheduling, TCP flows will starve).
