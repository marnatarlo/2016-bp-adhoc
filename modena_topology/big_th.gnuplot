reset
set terminal svg size 540,380 fname "Gill Sans" fsize 5 rounded dashed
#set terminal post eps size 10in,3.5in color font "Arial,20" linewidth 3 dashed dl 3

set termoption enhanced

# Line style for axes
set style line 80 lt 1
set style line 80 lt rgb "#000000"

# Line style for grid
set style line 81 lt 0  # no dashed
set style line 81 lt rgb "#808080" lw 0.5  # grey

#set grid back linestyle 81
set border 3 back linestyle 80 # Remove border on top and right.  These
			       # borders are useless and make it harder
			       # to see plotted lines near the border.
			       # Also, put it in grey; no need for so much
			       # emphasis on a border.

#set xtics nomirror
#set ytics nomirror


# Logscale?
#set log x
#set mytics 1000    # Makes logscale look good.
# End Logscale
#
# Line styles: try to pick pleasing colors,
# rather
# than strictly primary colors or hard-to-see
# colors
# like gnuplot default yellow.  Make the lines
# thick
# so theyre easy to see in small plots in
# papers.

# set style fill transparent solid 1.00 border 0
set style fill transparent pattern border

# set grid ytics
# set grid noxtics

set boxwidth 0.85

 set style line 1 lt 1 
 set style line 2 lt 1
 set style line 3 lt 1
 set style line 4 lt 1
 set style line 5 lt 1
 set style line 6 lt 1 

 set style line 1 lt rgb "#FFFF0000" lw 1
 set style line 2 lt rgb "#FF00A000" lw 1 
 set style line 3 lt rgb "#FFE75480" lw 1
 set style line 4 lt rgb "#FF0000A0" lw 1 
 set style line 5 lt rgb "#FF999900" lw 1 
 set style line 6 lt rgb "#FF0060D0" lw 1

#lenght of picture in key
set key samplen 2.5

#space added between key
set key width -2

#font size in key
set key font ",18"

#size picture in key
#set key spacing 0.5

#raw in key
#set key outside right center vertical
set key outside bottom center vertical maxrows 2
