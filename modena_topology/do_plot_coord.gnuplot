
IMG_LIST="ENB.png"
X=""
Y=""
IMG=""
storedata(x,y,index_img)=                               \
        (X=X.sprintf(" %f",x),                          \
        Y=Y.sprintf(" %f",y),                           \
        IMG=IMG." ".word(IMG_LIST,int(index_img)),y)

set xlabel "Field Length (m)"
set ylabel "Field Length (m)"

plot "coords.txt" u 1:(storedata($1,$2,1))
set output "modena_enb.svg"
plot [-200:4000][-300:3300] for [i=1:words(IMG)] word(IMG,i) binary filetype=png center=(word(Y,i),word(X,i)) dx=5 dy=5 with rgbimage notitle, "coords.txt" u ($2):($1-200):($3) with labels notitle
