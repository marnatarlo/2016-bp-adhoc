
This is the first work that extensively analyzes different TCP variants and
different backhaul routing strategies to carry LTE traffic over wireless mesh
networks. Previous work has been focused on adding multi-path TCP
(MPTCP)~\cite{ford2011architectural} extensions at the
transport layer. However, this would require modifications at the LTE mobile
clients and Internet servers. Besides, MPTCP is end-to-end and can
exploit the path diversity only at endpoints. Therefore, it can not help when
such diversity is located and dynamically exploited inside the backhaul
network, which is the primary focus of the analyzed routing algorithms.
Many works, such as~\cite{7796870}, use a wired network with no redundancy as
environment for comparing the different congestion control algorithms. For
instance, in many works the dumbbell topology is used; we aim to provide a
comparison in redundant topologies, with the support of specialized routing
protocols. For what regards mobile networks, a congestion control comparison is
performed in~\cite{6583408}, where the authors focus on three different
congestion control (Cubic, NewReno, and Westwood+) and investigate the relation
between the algorithms and the bufferbloat problem in commercial 3G and 4G
networks. Interestingly, their findings show how 3G and 3.5G networks highly
suffer from bufferbloat, and how aggressive congestion controls (such as Cubic)
significantly increase the response time to the users. In our paper, we also
include the routing protocol in the analysis, using redundant topologies. Our
objective is to demonstrate that the TCP congestion control algorithm is not the
only component to define the expected performance, standing out from the
previous literature. Moreover, using routing protocols and TCP congestion
controls as variables allowed us to see a big performance improvements, due to
the backpressure-based root of \emph{BP-MR per-flow}, even
without using custom layer approach (such as in~\cite{6971170}
or~\cite{al2015itcp}).

Wireless Mesh Networks topologies are not new in the academic world. When the
network is dense, especially in client meshing (i.e. nodes collaborate in
absence of a backhaul), the routing overhead can be very high.
One technique to reduce the overload added by the routing management
messages is to selectively enable or disable the routing functionality in nodes,
actually implementing a topology control~\cite{vazquez2015centrality}. 
On the contrary, we assume that the topology is fixed in the network
design phase, with our objective that is to maximize the usage of network
resources. Therefore, turning off a routing node is a possibility not
contemplated in this work.

Congestion algorithms comparison in an ad-hoc network has been performed
in~\cite{goyal2013performance}, but using only one routing protocol, Dynamic
State Routing (DSR). The results presented in the paper clearly indicate that
the DSR achieves maximum throughput, higher packet delivery ratio and a less
average end-to-end delay with TCP Vegas, a delay-based congestion control. This
is somehow against the main conviction that delay-based algorithms incur in low
delay but also low throughput: this confirms once more that, in a wireless
environment, is important to measure the performance by evaluating together
routing protocols and congestion control algorithms: different combination
provides always different results. In our case, the use of Vegas does not
give the best combined delay/throughput results with any of our routing
protocols.

TCP performance over the LTE RAN has been investigated in both simulated
environments and over real data. For the simulated environment, many works (such
as~\cite{6005714,abed2011behavior}) simulate the access network with simple
point-to-point links, with different properties, and so without taking into
account the complex dynamics of the Radio Resource Control (RRC) state machine
and the TCP protocol, thus invalidating the obtained results. Nguyen et al.
in~\cite{tcp-lte-nguyen} investigate the performance of TCP over the full RAN
stack, concluding that increasing the load in a cell can significantly throttle
the bandwidth available to a UE, thus increasing the experienced delay,
especially when the eNodeB maintains a large per-UE queue. This can invalidate
the estimated RTO value, causing unnecessary TCP timeouts even when no packets
are lost. Also, they concluded that radio-link handover could cause significant
performance degradation. Similar conclusions are drawn
in~\cite{lte-tcp-in-depth-real-networks}, where the authors analyze a
large-scale real LTE data set to study the impact of protocol and application
behaviors on the LTE network performance. For instance, they concluded that
some  TCP behaviors (such as not updating the RTT estimation using the
duplicate ACKs) could cause severe performance issues; also, the bandwidth
utilization ratio is usually below 50\% for large flows. The work provides
valuable insights on the interaction between TCP and LTE. However, the details
of the backhaul network are out of the analysis (e.g. the routing algorithm).
We aim to fill this hole by adding the analysis of different backhaul routing
protocols over many TCP variants assuming a constrained wireless mesh backhaul.

For analysis of TCP traffic over backpressure routing we refer
to~\cite{RadunovicHorizon}, that identify the packet reordering at the receiver
as the main issue with backpressure routing, and then proposes a delayed
reordering algorithm at the destination for keeping packet reordering to a
minimum. Other proposals use the MAC layer to perform such
scheduling, such as in~\cite{nawab2014fair}. While using the MAC layer ties the
proposal to a particular technology (in~\cite{nawab2014fair} is used an IEEE
802.11-based wireless mesh network), we believe that avoiding reordering is more
profitable than re-ordering packets in later stages. Under this light, we
proposed the \emph{BP-MR per-flow} variant.
Furthermore, talking about backpressure-based algorithms,
in~\cite{seferoglu2014tcp} it is shown that TCP experiences incompatibilities
with backpressure strategies that maintain per-flow queues, hence leading to
unfairness between flows. In contrast to this work, we analyze the performance
of \emph{BP-MR}, a distributed and scalable backpressure-based routing protocol
that maintains per-interface queues, which does not require changes at the TCP
layer.

The history of \emph{BP-MR} started with \emph{BP}~\cite{bp}, a self-organized
backpressure routing protocol that is a decentralized flavor of the original
centralized backpressure algorithm. For dealing with sparse networks,
Backpressure for Sparse Deployments (\emph{BS})~\cite{bs} included additional
extensions to \emph{BP}. In particular, \emph{BS} added a penalty function
able to overcome dead ends in a scalable and decentralized way. However,
\emph{BS} was designed to tackle sparse topologies where nodes are equipped
with a single backhaul radio and presented huge inefficiencies in
multi-radio deployments (i.e. with multiple backhaul interfaces). To mitigate
such inefficiencies, we proposed in~\cite{7396680} \emph{BP-MR}, used in this
paper and detailed for the sake of completeness in Section~\ref{sec:background}.

In our work, each routing node of a multi-hop path from the source to the
destination cooperates in sending information to its neighbor nodes. Under
such cooperative view, we can compare \emph{BP-MR} with other routing protocol
proposals. For instance, one of the most practical environment to evaluate these
proposals is a Wireless Sensor Networks (WSN) deployment.
In~\cite{razzaque2014qos} is presented a Distributed Adaptive Cooperative
Routing (DACR) protocol, that starts from a path created by Ad hoc On-Demand
Distance Vector (AODV) and then refined, hop by hop, by introducing delay and
energy constraints gathered through a reinforcement learning method. The main
difference with our work is that we use point-to-point links to model the
wireless network, instead of the point-to-multipoint typical of WSN. In
point-to-multipoint scenario, the quality of a link between two neighbors plays
a crucial role in the routing decision, while that quality in our environment is
fixed, and overtaken in importance by the queuing delays (not analyzed
in~\cite{razzaque2014qos}).
